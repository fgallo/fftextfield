
Pod::Spec.new do |s|

  s.name         = "FFTextField"
  s.version      = "1.0.3"
  s.summary      = "A Library that joins common features expected from a textfield in a unique class."
  s.homepage     = "https://FelipeGus@bitbucket.org/FelipeGus/fftextfield.git"
  s.license      = "MIT"
  s.author             = { "Felipe Figueiredo" => "felipegusfigueiredo@gmail.com" }
  s.social_media_url = 'https://twitter.com/ifegufi'
  s.platform     = :ios, "10.0"
  s.source = { :git => 'https://FelipeGus@bitbucket.org/FelipeGus/fftextfield.git', :tag => s.version }
  s.source_files  = "FFTextField", "FFTextField/**/*.{h,m,swift}"

  s.dependency 'SwiftMaskTextField'
  s.dependency 'SkyFloatingLabelTextField', '~> 3.4.0'

end
